
from django.http import HttpResponse

from django.shortcuts import render, redirect, get_object_or_404
# The from keyword allows importing of necessary Classes, methods and other items needed in our application from the "django.http" package while the "import" keyword defines what we are importing from the package
# from django.http import HttpResponse
# from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone
from django.contrib.auth.hashers import make_password

# Local Imports
from .models import ToDoItem, Events
from django.contrib.auth.models import User
from .forms import LoginForm, AddTaskForm, RegisterForm,AddEventForm

def index(request):
    todoitem_list = ToDoItem.objects.filter(user_id = request.user.id)
    events_list = Events.objects.filter(user_id = request.user.id)

    context = {
        "todoitem_list": todoitem_list,
        "events_list": events_list,
        "user": request.user
    }
    return render(request, "todolist/index.html", context)

def todoitem(request, todoitem_id):
    # The model_to_dict method allows to convert models into dictionaries
    # The .get() Model method allows to retrieve a specific record using it's primary key (pk)
    todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)
    # Instead of creating a context, we can pass the dictionary directly as the context of the view
    return render(request, "todolist/todoitem.html", model_to_dict(todoitem))

def events(request, event_id):
    event = get_object_or_404(Events, pk=event_id)
    return render(request, "todolist/events.html", model_to_dict(event))

def register(request):

    # users = User.objects.all()
    # is_user_registered = False
    # context = {
    #     "is_user_registered": is_user_registered
    # }

    # for indiv_user in users:
    #     if indiv_user.username == "johndoe":
    #         is_user_registered = True
    #         break

    # if is_user_registered == False:
    #     user = User()
    #     user.username = "johndoe"
    #     user.first_name = "John"
    #     user.last_name = "Doe"
    #     user.email = "john@mail.com"
    #     # The set_password method is used to ensure that the password is hashed using Django's authentication framework
    #     user.set_password("john1234")
    #     user.is_staff = False
    #     user.is_active = True
    #     user.save()
    #     context = {
    #         "first_name": user.first_name,
    #         "last_name": user.last_name
    #     }

    context = {}

    if request.method == "POST":
        
        form = RegisterForm(request.POST)

        if form.is_valid() == False:

            form = RegisterForm()

        else:

            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            password1 = form.cleaned_data['password1']
            password2 = form.cleaned_data['password2']
            duplicate = User.objects.filter(email = email)

            if not duplicate and password1 == password2:
                User.objects.create(username = username, first_name = first_name, last_name = last_name, email = email, password = make_password(password1), is_staff=False, is_active=True)
                return redirect("todolist:login")

            else:

                context = { "error" : True }


    return render(request, "todolist/register.html", context)

def change_password(request):

    is_user_authenticated = False

    user = authenticate(username="johndoe", password="john1234")
    print(user)
    if user is not None:
        authenticated_user = User.objects.get(username='johndoe')
        authenticated_user.set_password("johndoe1")
        authenticated_user.save()
        is_user_authenticated = True
    context = {
        "is_user_authenticated": is_user_authenticated
    }

    return render(request, "todolist/change_password.html", context)

def login_view(request):

    context = {}

    if request.method == "POST":
        # Create a form instance and populate it with the data from the request
        form = LoginForm(request.POST)
        # Check whether the data is valid
        # Runs validation routines for all the form fields and returns true and palces the form's data in the cleaned data attribute
        if form.is_valid() ==False:
            # Return a blank login form
            form = LoginForm()
        else:
            # Receives the information from the form
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username = username, password = password)
            context = {
                "username" : username,
                "password" : password
            }
            if user is not None:
                login(request, user)
                return redirect ("todolist:index")
            else:
                context = {
                    "error" : True
                }
    return render(request, "todolist/login.html", context)

    # username = "johndoe"
    # password = "johndoe1"
    # user = authenticate(username=username, password=password)
    # context = {
    #     "is_user_authenticated": False
    # }
    # print(user)
    # if user is not None:
    #     # Saves the user’s ID in the session using Django's session framework
    #     login(request, user)
    #     return redirect("todolist:index")
    # else:
    #     return render(request, "todolist/login.html", context)

def logout_view(request):
    logout(request)
    return redirect("todolist:index")


def add_task(request):
    context = {}

    if request.method == "POST":
        form = AddTaskForm(request.POST)
        if form.is_valid() == False:
            form = AddTaskForm()

        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            duplicate = ToDoItem.objects.filter(task_name = task_name)

            if not duplicate:
                ToDoItem.objects.create(task_name=task_name, description=description, date_created=timezone.now(), user_id = request.user.id)
                return redirect("todolist:index")

            else:

                context = { "error" : True }

    return render(request, "todolist/add_task.html", context)

def add_event(request):
    context = {}

    if request.method == "POST":
        form = AddEventForm(request.POST)
        if form.is_valid() == False:
            form = AddEventForm()

        else:
            event_name = form.cleaned_data['event_name']
            description = form.cleaned_data['description']
            duplicate = Events.objects.filter(event_name = event_name)

            if not duplicate:
                Events.objects.create(event_name=event_name, description=description, user_id = request.user.id)
                return redirect("todolist:index")

            else:

                context = { "error" : True }

    return render(request, "todolist/add_event.html", context)